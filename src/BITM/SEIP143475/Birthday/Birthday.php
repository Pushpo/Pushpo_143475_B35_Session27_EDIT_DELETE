<?php

namespace App\Birthday;
use App\Message\Message;
use App\Model\Database as DB;

use App\Utility\Utility;
use PDO;

class Birthday extends DB
{
    public $id = "";
    public $name = "";
    public $birthday = "";

    public function __construct()
    {
        parent::__construct();
    }


    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];
        }

        if (array_key_exists('birthday', $postVariableData)) {
            $this->birthday = $postVariableData['birthday'];
        }


    }

    public function store()
    {
        $arrData = array($this->name, $this->birthday);
        $sql = "Insert INTO birthday(name,birthday) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::setMessage("Success! Data has been inserted successfully! :) ");
        else
            Message::setMessage("Failed! Data has not been inserted successfully! :( ");
        Utility::redirect('create.php');
    }
}

   /* public function index()
    {
        echo "index birthday";
    }
*/

